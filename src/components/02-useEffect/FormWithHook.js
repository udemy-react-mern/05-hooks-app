import React from 'react'
import './effects.css';
import Message from './Message';
import { useForm } from '../../hooks/useForm';

const FormWithHook = () => {

  const [formValues, handleChange] = useForm({
    name: '',
    email: '',
    password: ''
  });

  const { name, email, password } = formValues;


  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formValues);
  }



  return (
    <form onSubmit={handleSubmit}>
      <h1>useEffects</h1>
      <div className="form-group">
        <input 
          type="text"
          name="name"
          className="form-control"
          placeholder="Nombre"
          autoComplete="off"
          value={name}
          onChange={handleChange}
        />
      </div>
      <div className="form-group">
        <input 
          type="email"
          name="email"
          className="form-control"
          placeholder="email@gmail.com"
          autoComplete="off"
          value={email}
          onChange={handleChange}
        />
      </div>
      <div className="form-group">
        <input 
          type="password"
          name="password"
          className="form-control"
          placeholder="Password"
          value={password}
          onChange={handleChange}
        />
      </div>
      <button type="submit" className="btn btn-primary">Enviar Formulario</button>

      {(name==='123') && <Message />}
    </form>
  )
};

export default FormWithHook;
