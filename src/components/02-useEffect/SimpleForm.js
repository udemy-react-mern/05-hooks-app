import React, { useEffect, useState } from 'react'
import './effects.css';
import Message from './Message';

const SimpleForm = () => {

  const [formState, setFormState] = useState({
    name: '',
    email: ''
  });

  const { name, email } = formState;

  const handleChange = ({target})=> {
    setFormState({...formState, [target.name]: target.value})
  };


  // Con el parametro de [] solo se ejecuta una vez
  useEffect( () => {
    console.log('holis :D');
  }, []); 

  // Solo se ejecuta cambia formulario 
  useEffect( () => {
    console.log('formState Cambió');
  }); 
  // Solo se ejecuta cuando cambia email
  useEffect( () => {
    console.log('Email Cambió');
  }, [email]); 


  return (
    <>
      <h1>useEffects</h1>
      <div className="form-group">
        <input 
          type="text"
          name="name"
          className="form-control"
          placeholder="Nombre"
          autoComplete="off"
          value={name}
          onChange={handleChange}
        />
      </div>
      <div className="form-group">
        <input 
          type="email"
          name="email"
          className="form-control"
          placeholder="email@gmail.com"
          autoComplete="off"
          value={email}
          onChange={handleChange}
        />
      </div>

      {(name==='123') && <Message />}
    </>
  )
};

export default SimpleForm;
