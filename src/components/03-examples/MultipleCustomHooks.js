import React from 'react';
import '../02-useEffect/effects.css';
import { useFetch } from '../../hooks/useFetch';
import { useCounter } from '../../hooks/useCounter';

const MultipleCustomHooks = () => {

  const {state:counter, increment } = useCounter(1);

  const { loading, data} = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`);
  const { author, quote } = !!data && data[0];

    
  return (
    <div>
      <h1>Breaking Bad Quotes {counter}</h1>
      {
        loading ? 
          (
            <div className="alert alert-info text-center">
              Loading...
            </div>
          )
        :
          (
            <blockquote className="blockquote text-right">
              <p className="mb-0">{quote}</p>
          <footer className="blockquote-footer">{author}</footer>
            </blockquote>
          )
      }
      <br/>
      <button className="btn btn-primary" onClick={() => increment(1)}>Ver más</button>
    </div>
  )
};

export default MultipleCustomHooks;
