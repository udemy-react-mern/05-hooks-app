import React, { memo } from 'react'

/**
 * Se usa solo cuando sus propiedades cambian
 */
const Small = memo(({value}) => {
  console.log('AQUO');
  return ( <small>{value}</small>)
});

export default Small;