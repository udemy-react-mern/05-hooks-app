import React, { useState, useMemo } from 'react'
import { useCounter } from '../../hooks/useCounter';
import { procesoPesado } from '../../helpers/procesoPesado';

const MemoHook = () => {
  const {state:counter, increment} = useCounter(100);
  const [show, setShow] = useState(true);


  const memoProcesoPedaso = useMemo(() => procesoPesado(counter), [counter]);

  return (
    <div>
      <h1>Memo Hook</h1>
      <h2>Counter <small>{counter}</small></h2>
      <hr/>
      <p>{memoProcesoPedaso}</p>
      <button
        className="btn btn-primary"
        onClick={() => {
          console.log('holis');
          increment(1);
        }}
      >
        +1
      </button>
      <button
        className="btn btn-outline-primary ml-3"
        onClick={() => setShow(!show)}
      >
        show / hide {JSON.stringify(show)}
      </button>
    </div>
  )
};

export default MemoHook;