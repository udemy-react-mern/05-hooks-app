import React, { useContext } from 'react';
import { UserContext } from './UserContext';

const LoginScreen = () => {
  const {setUser} = useContext(UserContext);

  const userData = {
    id:12345,
    name:'Paulina'
  };
  
  return (
    <div>
      <h1>Login</h1>
      <hr/>
      <button
        className="btn btn-primary"
        onClick={ () => setUser(userData)}
      >
        Iniciar Sesión
      </button>
    </div>
  )
};
export default LoginScreen;
