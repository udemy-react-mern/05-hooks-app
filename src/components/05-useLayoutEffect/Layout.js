import React, { useLayoutEffect, useRef, useState } from 'react';
import './Layout.css';
import { useFetch } from '../../hooks/useFetch';
import { useCounter } from '../../hooks/useCounter';

const Layout = () => {

  const {state:counter, increment } = useCounter(1);

  const { data } = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`);
  const { author, quote } = !!data && data[0];

  const [boxSize, setBoxSize] = useState({})
  const pTag = useRef();

  useLayoutEffect(() => {
    setBoxSize(pTag.current.getBoundingClientRect());
  }, [quote]);
    
  return (
    <div>
      <h1>Layout Effect</h1>
      <blockquote className="blockquote text-right">
        <p className="mb-0" ref={pTag}>{quote}</p>
        <footer className="blockquote-footer">{author}</footer>
      </blockquote>
      <br/>
      <button className="btn btn-primary" onClick={() => increment(1)}>Ver más</button>

      <pre>
        { JSON.stringify(boxSize, null, 3) }
      </pre>
    </div>
  )
};

export default Layout;
