import React from 'react';
import '@testing-library/react';
import { shallow } from 'enzyme';
import RealExampleRef from '../../../components/04-useRef/RealExampleRef';

describe('Pruebas de <RealExampleRef/>', () => {
  const wrapper = shallow(<RealExampleRef />);

  test('debe cargarse correctamente', () => {
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('MultipleCustomHooks').exists()).toBe(false);
  });

  test('debe cargarse el componente <MultipleCustomHooks />', () => {
    wrapper.find('button').simulate('click');
    expect(wrapper.find('MultipleCustomHooks').exists()).toBe(true);
  });
});
