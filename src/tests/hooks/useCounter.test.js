import { renderHook, act } from '@testing-library/react-hooks';
import { useCounter } from '../../hooks/useCounter';

describe('Pruebas en useCounter', () => {
  it('debe regresar valores por defecto', () => {
    const { result } = renderHook(() => useCounter());

    expect(result.current.state).toBe(10);
    expect(typeof result.current.increment).toBe('function');
    expect(typeof result.current.decrement).toBe('function');
    expect(typeof result.current.reset).toBe('function');
  });

  it('debe de incrementar el counter en 1', () => {
    const { result } = renderHook(() => useCounter());

    const { increment } =result.current;

    act(() => {
      increment();
    });

    const { state } = result.current;

    expect(state).toBe(11);
  });

  it('debe de decrementar el counter en 1', () => {
    const { result } = renderHook(() => useCounter());

    const { decrement } =result.current;

    act(() => {
      decrement();
    });

    const { state } = result.current;
    expect(state).toBe(9);
  });

  it('debe restablecer al valor por defecto', () => {
    const { result } = renderHook(() => useCounter());

    const { decrement, reset } =result.current;

    act(() => {
      decrement();
      reset();
    });
    
    const { state } = result.current;
    
    expect(state).toBe(10);  
  });
});
