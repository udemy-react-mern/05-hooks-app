import { renderHook } from '@testing-library/react-hooks';
import { useFetch } from '../../hooks/useFetch';
import '@testing-library/react';

describe('Pruebas del hook useFetch', () => {
  test('debe traer la informacion por defecto ', () => {
    const { result } = renderHook(() => useFetch(`https://www.breakingbadapi.com/api/quotes/1`));
    const { data, loading, error } = result.current;

    expect(data).toBe(null);
    expect(loading).toBe(true);
    expect(error).toBe(null);
  });

  test('debe traer la info deseada', async() => {
    const { result, waitForNextUpdate } = renderHook(() => useFetch(`https://www.breakingbadapi.com/api/quotes/1`));
    await waitForNextUpdate();
    const { data, loading, error } = result.current;
    expect(data.length).toBe(1);
    expect(loading).toBe(false);
    expect(error).toBe(null);
  });

  test('debe manejar el error desde el catch', async() => {
    const { result, waitForNextUpdate } = renderHook(() => useFetch(`https://reqres.in/apid/users`));
    await waitForNextUpdate();
    const { data, loading, error } = result.current;

    expect(data).toBe(null);
    expect(loading).toBe(false);
    expect(error).toBe('No se pudo cargar la info');
  });
  
});
