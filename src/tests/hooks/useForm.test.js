import { renderHook, act } from '@testing-library/react-hooks';
import { useForm } from '../../hooks/useForm';

describe('Pruebas en useForm', () => {
  const initialForm = {
    email: 'paaulinagp@gmail.com',
    name: 'Paulina'
  };
  const target = {
    target: {
      name: 'name',
      value:'Paulina Guerrero'
    }
  };

  test('debe de regresar un formulario por defecto ', () => {
    const { result } = renderHook(() => useForm(initialForm));
    const [formValues, handleChange, reset] = result.current;
    expect(formValues).toBe(initialForm);
    expect(typeof handleChange).toBe('function');
    expect(typeof reset).toBe('function');

  });

  test('debe de cambiar el valor del formulario (name) ', () => {
    const { result } = renderHook(() => useForm(initialForm));
    const [,handleChange] = result.current;

    act(() => {
      handleChange({...target});
    });
    const [formValues] = result.current;
    expect(formValues).toEqual({...initialForm, name: 'Paulina Guerrero'});

  });
  test('debe de restablecer el formulario con RESET ', () => {
    const { result } = renderHook(() => useForm(initialForm));
    const [,handleChange, reset] = result.current;

    act(() => {
      handleChange({...target});
      reset();
    });
    const [formValues] = result.current;
    expect(formValues).toEqual(initialForm);
  });
});
