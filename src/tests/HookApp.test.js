import React from 'react';
import { shallow } from 'enzyme';
import HookApp from '../HookApp';
 
describe('<HookApp/>', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <HookApp />
    );
   
    expect(wrapper).toMatchSnapshot();
  });
})
