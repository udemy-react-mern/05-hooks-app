import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
// import CounterApp from './components/01-useState/CounterApp';
// import CounterWithHook from './components/01-useState/CounterWithHook';
// import SimpleForm from './components/02-useEffect/SimpleForm';
// import FormWithHook from './components/02-useEffect/FormWithHook';
// import MultipleCustomHooks from './components/03-examples/MultipleCustomHooks';
// import FocusScreen from './components/04-useRef/FocusScreen';
// import RealExampleRef from './components/04-useRef/RealExampleRef';
// import Layout from './components/05-useLayoutEffect/Layout';
// import Memorize from './components/06-memos/Memorize';
// import MemoHook from './components/06-memos/MemoHook';
// import CallbackHook from './components/06-memos/CallbackHook';
//import TodoApp from './components/08-useReducer/TodoApp';
import MainApp from './components/09-useContext/MainApp';

ReactDOM.render(
  <React.StrictMode>
    <MainApp />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
